﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class limb : MonoBehaviour
{
	public static limb currentdrag;
	public limbphsx[] phsx;
	[SerializeField] limbtype type;
	[SerializeField] bool handleg = false;
	int stage = 0;
	Vector3 prevmousepos = Vector2.zero;
	Vector3 prevobjpos = Vector2.zero;
	int hover;
	int idd = 1;
	float amnt = 0;
	float startrot;

	void Start()
	{
		Color clr = Color.HSVToRGB(Random.value, .6f, 1);
		phsx[0].clr(clr);
		if(phsx.Length == 2)
		{
			phsx[1].clr(clr);
			transform.localEulerAngles = new Vector3(0,0,Random.Range(0f,360f));
			phsx[1].transform.localEulerAngles = new Vector3(0,0,Random.Range(-25f, 25f));
		}
	}

	void Update()
	{
		switch(stage)
		{
			case 0:
				transform.localPosition = new Vector3(transform.localPosition.x - Time.deltaTime*25, transform.localPosition.y, transform.localPosition.z);
				if(transform.localPosition.x <= -90f) Destroy(gameObject);
				break;
			case 1:
				if(timerr.timeleft <= 0) {
					stage = 2;
					Destroy(gameObject, 2);
				}

				Vector2 diff = new Vector2(
					((Input.mousePosition.x - prevmousepos.x)*160.0f)/(float)Screen.width,
					((Input.mousePosition.y - prevmousepos.y)*90.0f)/(float)Screen.height);

				if(phsx.Length == 2)
				{
					amnt = Mathf.Clamp01(amnt + Time.deltaTime);
					phsx[idd].transform.localEulerAngles = new Vector3(0, 0, Mathf.Sin(Time.time*6)*10*amnt + startrot);
				}

				transform.localPosition = new Vector3(prevobjpos.x + diff.x, prevobjpos.y + diff.y, prevobjpos.z);
				break;
			case 2:
				float scl = transform.localScale.x*(1 - Mathf.Min(Time.deltaTime*15, 1));
				transform.localScale = new Vector3(scl,scl,scl);
				break;
		}
	}

	public void clicke(bool state, int id)
	{
		idd = (phsx.Length - 1 - id);
		startrot = phsx[idd].transform.localEulerAngles.z;

		if(state)
		{
			if(stage == 0)
			{
				stage = 1;
				uioperator.uiop.squishplay();
				prevmousepos = Input.mousePosition;
				prevobjpos = transform.localPosition;
				currentdrag = this;
				phsx[0].hvr(false);
				if(phsx.Length == 2) phsx[1].hvr(false);
			}
		}
		else if(stage == 1)
		{
			currentdrag = null;
			if(droparea.currentdrop == null && facedrop.currentdrop == null)
			{
				stage = 2;
				Destroy(gameObject, 2);
			}
			else
			{
				if(phsx.Length == 2)
				{
					transform.position = droparea.currentdrop.transform.position;
					transform.rotation = droparea.currentdrop.transform.rotation;
					transform.position = new Vector3(
						transform.position.x + (transform.position.x - phsx[0].transform.position.x),
						transform.position.y + (transform.position.y - phsx[0].transform.position.y),
						transform.position.z);
					transform.SetParent(droparea.currentdrop.transform.parent, true);
					transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z + (type == limbtype.body ? 1 : -1));

					if(droparea.currentdrop.lookingfor == limbtype.body && type == limbtype.body)
					{
						droparea.currentdrop.nearestbody = gameObject.GetComponent<body>();
						droparea.currentdrop.nearestbody.nearestoperator = droparea.currentdrop.nearestperson;
					}
					
					if(handleg)
						droparea.currentdrop.donee(droparea.currentdrop.lookingfor == limbtype.hand || droparea.currentdrop.lookingfor == limbtype.leg);
					else
						droparea.currentdrop.donee(type == droparea.currentdrop.lookingfor);
				} else {
					transform.SetParent(facedrop.currentdrop.fcthng, true);
					facedrop.currentdrop.donee(type, transform.localPosition);
				}
				stage = 3;
				enabled = false;
			}
		}
	}

	public void hovery(bool state)
	{
		if(stage >= 2) return;
		int prevhover = hover;
		hover += state ? 1 : -1;
		if(stage != 0) return;
		if(hover == 0)
		{
			phsx[0].hvr(false);
			if(phsx.Length == 2) phsx[1].hvr(false);
		} else if (hover == 1 && prevhover == 0) {
			phsx[0].hvr(true);
			if(phsx.Length == 2) phsx[1].hvr(true);
		}
	}
}

public enum limbtype
{
	body = 0,
	hand = 1,
	leg = 2,
	mouth = 3,
	nose = 4,
	eye = 5,
	hair = 6
}
