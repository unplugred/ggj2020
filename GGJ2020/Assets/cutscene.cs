﻿using System;
using UnityEngine.UI;
using UnityEngine;

public class cutscene : MonoBehaviour
{
	[SerializeField] Image img;
	[SerializeField] slide[] slides;
	[SerializeField] GameObject game;
	[SerializeField] AudioSource sauce;
	int currentslide = 0;

	void Start()
	{
		img.sprite = slides[0].spr;
	}

	void Update()
	{
		if(sauce.time >= slides[currentslide].im)
		{
			currentslide++;
			if(currentslide >= slides.Length)
			{
				game.SetActive(true);
				gameObject.SetActive(false);
			}
			else img.sprite = slides[currentslide].spr;
		}
	}
}

[System.Serializable]
public class slide {
	public Sprite spr;
	public float im;
}