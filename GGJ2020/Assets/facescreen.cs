﻿using UnityEngine.UI;
using UnityEngine;

public class facescreen : MonoBehaviour
{
	int state = 0;
	float anim = 0;
	[SerializeField] int slot = 0;
	[SerializeField] facedrop fcdrp;
	bool successs;
	Transform facedupe;

	void OnEnable()
	{
		state = 0;
		fcdrp.enabled = true;
		if(slot == 0) fcdrp.papa = bodyoperator.slot1;
		else fcdrp.papa = bodyoperator.slot2;
		fcdrp.img.color = fcdrp.papa.head.color;
		facedupe = Instantiate(fcdrp.fcthng.gameObject, Vector3.zero, Quaternion.identity, fcdrp.transform).transform;
		facedupe.localPosition = new Vector3(0, 0, -1);
	}

	void Update()
	{
		switch(state)
		{
			case 0:
				anim = Mathf.Clamp01(anim + Time.deltaTime*.3f);
				float animm = (float)scr_easings.EaseOutCubic(anim);
				if(slot == 0)
					transform.localPosition = new Vector3(Mathf.Lerp(165f, 80f, animm), transform.localPosition.y, transform.localPosition.z);
				else
					transform.localPosition = new Vector3(Mathf.Lerp(-165f, -80f, animm), transform.localPosition.y, transform.localPosition.z);
				if(anim == 1) state = 2;
				break;
			case 1:
				anim = Mathf.Clamp01(anim - Time.deltaTime*.5f);
				animm = (float)scr_easings.EaseOutCubic(anim);
				if(slot == 0)
					transform.localPosition = new Vector3(Mathf.Lerp(165f, 80f, animm), transform.localPosition.y, transform.localPosition.z);
				else
					transform.localPosition = new Vector3(Mathf.Lerp(-165f, -80f, animm), transform.localPosition.y, transform.localPosition.z);
				if(anim == 0)
				{
					if(successs) fcdrp.fcthng.transform.SetParent(fcdrp.papa.head.transform, false);
					else Destroy(fcdrp.fcthng.gameObject);
					fcdrp.fcthng = facedupe;
					gameObject.SetActive(false);
				}
				break;
		}
	}

	public void facecomplete(bool success)
	{
		successs = success;
		state = 1;
	}
}
