﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cutsceneenable : MonoBehaviour
{
	[SerializeField] GameObject game;
	[SerializeField] GameObject cutscene;
	static bool cutscened = false;

	void Start()
	{
		if(cutscened) game.SetActive(true);
		else cutscene.SetActive(true);
		cutscened = true;
	}
}
