﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

static public class scr_easings
{
    /// <summary>
    /// Constant Pi.
    /// </summary>
    //private const double PI = Math.PI;

    /// <summary>
    /// Constant Pi / 2.
    /// </summary>
    private const double HALFPI = Mathf.PI * 0.5f;

    /// <summary>
    /// Easing Functions enumeration
    /// </summary>
    public enum Functions
    {
        Linear,
        EaseInQuad,
        EaseOutQuad,
        EaseInOutQuad,
        EaseInCubic,
        EaseOutCubic,
        EaseInOutCubic,
        EaseInQuart,
        EaseOutQuart,
        EaseInOutQuart,
        EaseInQuint,
        EaseOutQuint,
        EaseInOutQuint,
        EaseInSine,
        EaseOutSine,
        EaseInOutSine,
        EaseInCirc,
        EaseOutCirc,
        EaseInOutCirc,
        EaseInExpo,
        EaseOutExpo,
        EaseInOutExpo,
        EaseInElastic,
        EaseOutElastic,
        EaseInOutElastic,
        EaseInBack,
        EaseOutBack,
        EaseInOutBack,
        EaseInBounce,
        EaseOutBounce,
        EaseInOutBounce
    }

    /// <summary>
    /// Interpolate using the specified function.
    /// </summary>
    static public double Interpolate(double p, Functions function)
    {
        switch (function)
        {
            default:
            case Functions.Linear: return Linear(p);
            case Functions.EaseOutQuad: return EaseOutQuad(p);
            case Functions.EaseInQuad: return EaseInQuad(p);
            case Functions.EaseInOutQuad: return EaseInOutQuad(p);
            case Functions.EaseInCubic: return EaseInCubic(p);
            case Functions.EaseOutCubic: return EaseOutCubic(p);
            case Functions.EaseInOutCubic: return EaseInOutCubic(p);
            case Functions.EaseInQuart: return EaseInQuart(p);
            case Functions.EaseOutQuart: return EaseOutQuart(p);
            case Functions.EaseInOutQuart: return EaseInOutQuart(p);
            case Functions.EaseInQuint: return EaseInQuint(p);
            case Functions.EaseOutQuint: return EaseOutQuint(p);
            case Functions.EaseInOutQuint: return EaseInOutQuint(p);
            case Functions.EaseInSine: return EaseInSine(p);
            case Functions.EaseOutSine: return EaseOutSine(p);
            case Functions.EaseInOutSine: return EaseInOutSine(p);
            case Functions.EaseInCirc: return EaseInCirc(p);
            case Functions.EaseOutCirc: return EaseOutCirc(p);
            case Functions.EaseInOutCirc: return EaseInOutCirc(p);
            case Functions.EaseInExpo: return EaseInExpo(p);
            case Functions.EaseOutExpo: return EaseOutExpo(p);
            case Functions.EaseInOutExpo: return EaseInOutExpo(p);
            case Functions.EaseInElastic: return EaseInElastic(p);
            case Functions.EaseOutElastic: return EaseOutElastic(p);
            case Functions.EaseInOutElastic: return EaseInOutElastic(p);
            case Functions.EaseInBack: return EaseInBack(p);
            case Functions.EaseOutBack: return EaseOutBack(p);
            case Functions.EaseInOutBack: return EaseInOutBack(p);
            case Functions.EaseInBounce: return EaseInBounce(p);
            case Functions.EaseOutBounce: return EaseOutBounce(p);
            case Functions.EaseInOutBounce: return EaseInOutBounce(p);
        }
    }

    /// <summary>
    /// Modeled after the line y = x
    /// </summary>
    static public double Linear(double p)
    {
        return p;
    }

    /// <summary>
    /// Modeled after the parabola y = x^2
    /// </summary>
    static public double EaseInQuad(double p)
    {
        return p * p;
    }

    /// <summary>
    /// Modeled after the parabola y = -x^2 + 2x
    /// </summary>
    static public double EaseOutQuad(double p)
    {
        return -(p * (p - 2));
    }

    /// <summary>
    /// Modeled after the piecewise quadratic
    /// y = (1/2)((2x)^2)             ; [0, 0.5)
    /// y = -(1/2)((2x-1)*(2x-3) - 1) ; [0.5, 1]
    /// </summary>
    static public double EaseInOutQuad(double p)
    {
        if (p < 0.5f)
        {
            return 2 * p * p;
        }
        else
        {
            return (-2 * p * p) + (4 * p) - 1;
        }
    }

    /// <summary>
    /// Modeled after the cubic y = x^3
    /// </summary>
    static public double EaseInCubic(double p)
    {
        return p * p * p;
    }

    /// <summary>
    /// Modeled after the cubic y = (x - 1)^3 + 1
    /// </summary>
    static public double EaseOutCubic(double p)
    {
        double f = (p - 1);
        return f * f * f + 1;
    }

    /// <summary>	
    /// Modeled after the piecewise cubic
    /// y = (1/2)((2x)^3)       ; [0, 0.5)
    /// y = (1/2)((2x-2)^3 + 2) ; [0.5, 1]
    /// </summary>
    static public double EaseInOutCubic(double p)
    {
        if (p < 0.5f)
        {
            return 4 * p * p * p;
        }
        else
        {
            double f = ((2 * p) - 2);
            return 0.5f * f * f * f + 1;
        }
    }

    /// <summary>
    /// Modeled after the quartic x^4
    /// </summary>
    static public double EaseInQuart(double p)
    {
        return p * p * p * p;
    }

    /// <summary>
    /// Modeled after the quartic y = 1 - (x - 1)^4
    /// </summary>
    static public double EaseOutQuart(double p)
    {
        double f = (p - 1);
        return f * f * f * (1 - p) + 1;
    }

    /// <summary>
    // Modeled after the piecewise quartic
    // y = (1/2)((2x)^4)        ; [0, 0.5)
    // y = -(1/2)((2x-2)^4 - 2) ; [0.5, 1]
    /// </summary>
    static public double EaseInOutQuart(double p)
    {
        if (p < 0.5f)
        {
            return 8 * p * p * p * p;
        }
        else
        {
            double f = (p - 1);
            return -8 * f * f * f * f + 1;
        }
    }

    /// <summary>
    /// Modeled after the quintic y = x^5
    /// </summary>
    static public double EaseInQuint(double p)
    {
        return p * p * p * p * p;
    }

    /// <summary>
    /// Modeled after the quintic y = (x - 1)^5 + 1
    /// </summary>
    static public double EaseOutQuint(double p)
    {
        double f = (p - 1);
        return f * f * f * f * f + 1;
    }

    /// <summary>
    /// Modeled after the piecewise quintic
    /// y = (1/2)((2x)^5)       ; [0, 0.5)
    /// y = (1/2)((2x-2)^5 + 2) ; [0.5, 1]
    /// </summary>
    static public double EaseInOutQuint(double p)
    {
        if (p < 0.5f)
        {
            return 16 * p * p * p * p * p;
        }
        else
        {
            double f = ((2 * p) - 2);
            return 0.5f * f * f * f * f * f + 1;
        }
    }

    /// <summary>
    /// Modeled after quarter-cycle of sine wave
    /// </summary>
    static public double EaseInSine(double p)
    {
        return Math.Sin((p - 1) * HALFPI) + 1;
    }

    /// <summary>
    /// Modeled after quarter-cycle of sine wave (different phase)
    /// </summary>
    static public double EaseOutSine(double p)
    {
        return Math.Sin(p * HALFPI);
    }

    /// <summary>
    /// Modeled after half sine wave
    /// </summary>
    static public double EaseInOutSine(double p)
    {
		return 0.5f * (1 - Math.Cos(p * Mathf.PI));
    }

    /// <summary>
    /// Modeled after shifted quadrant IV of unit circle
    /// </summary>
    static public double EaseInCirc(double p)
    {
        return 1 - Math.Sqrt(1 - (p * p));
    }

    /// <summary>
    /// Modeled after shifted quadrant II of unit circle
    /// </summary>
    static public double EaseOutCirc(double p)
    {
        return Math.Sqrt((2 - p) * p);
    }

    /// <summary>	
    /// Modeled after the piecewise circular function
    /// y = (1/2)(1 - Math.Sqrt(1 - 4x^2))           ; [0, 0.5)
    /// y = (1/2)(Math.Sqrt(-(2x - 3)*(2x - 1)) + 1) ; [0.5, 1]
    /// </summary>
    static public double EaseInOutCirc(double p)
    {
        if (p < 0.5f)
        {
            return 0.5f * (1 - Math.Sqrt(1 - 4 * (p * p)));
        }
        else
        {
            return 0.5f * (Math.Sqrt(-((2 * p) - 3) * ((2 * p) - 1)) + 1);
        }
    }

    /// <summary>
    /// Modeled after the exponential function y = 2^(10(x - 1))
    /// </summary>
    static public double EaseInExpo(double p)
    {
        return (p == 0.0f) ? p : Math.Pow(2, 10 * (p - 1));
    }

    /// <summary>
    /// Modeled after the exponential function y = -2^(-10x) + 1
    /// </summary>
    static public double EaseOutExpo(double p)
    {
        return (p == 1.0f) ? p : 1 - Math.Pow(2, -10 * p);
    }

    /// <summary>
    /// Modeled after the piecewise exponential
    /// y = (1/2)2^(10(2x - 1))         ; [0,0.5)
    /// y = -(1/2)*2^(-10(2x - 1))) + 1 ; [0.5,1]
    /// </summary>
    static public double EaseInOutExpo(double p)
    {
        if (p == 0.0 || p == 1.0) return p;

        if (p < 0.5f)
        {
            return 0.5f * Math.Pow(2, (20 * p) - 10);
        }
        else
        {
            return -0.5f * Math.Pow(2, (-20 * p) + 10) + 1;
        }
    }

    /// <summary>
    /// Modeled after the damped sine wave y = sin(13pi/2*x)*Math.Pow(2, 10 * (x - 1))
    /// </summary>
    static public double EaseInElastic(double p)
    {
        return Math.Sin(13 * HALFPI * p) * Math.Pow(2, 10 * (p - 1));
    }

    /// <summary>
    /// Modeled after the damped sine wave y = sin(-13pi/2*(x + 1))*Math.Pow(2, -10x) + 1
    /// </summary>
    static public double EaseOutElastic(double p)
    {
        return Math.Sin(-13 * HALFPI * (p + 1)) * Math.Pow(2, -10 * p) + 1;
    }

    /// <summary>
    /// Modeled after the piecewise exponentially-damped sine wave:
    /// y = (1/2)*sin(13pi/2*(2*x))*Math.Pow(2, 10 * ((2*x) - 1))      ; [0,0.5)
    /// y = (1/2)*(sin(-13pi/2*((2x-1)+1))*Math.Pow(2,-10(2*x-1)) + 2) ; [0.5, 1]
    /// </summary>
    static public double EaseInOutElastic(double p)
    {
        if (p < 0.5f)
        {
            return 0.5f * Math.Sin(13 * HALFPI * (2 * p)) * Math.Pow(2, 10 * ((2 * p) - 1));
        }
        else
        {
            return 0.5f * (Math.Sin(-13 * HALFPI * ((2 * p - 1) + 1)) * Math.Pow(2, -10 * (2 * p - 1)) + 2);
        }
    }

    /// <summary>
    /// Modeled after the overshooting cubic y = x^3-x*sin(x*pi)
    /// </summary>
    static public double EaseInBack(double p)
    {
		return p * p * p - p * Math.Sin(p *  Mathf.PI);
    }

    /// <summary>
    /// Modeled after overshooting cubic y = 1-((1-x)^3-(1-x)*sin((1-x)*pi))
    /// </summary>	
    static public double EaseOutBack(double p)
    {
        double f = (1 - p);
		return 1 - (f * f * f - f * Math.Sin(f * Mathf.PI));
    }

    /// <summary>
    /// Modeled after the piecewise overshooting cubic function:
    /// y = (1/2)*((2x)^3-(2x)*sin(2*x*pi))           ; [0, 0.5)
    /// y = (1/2)*(1-((1-x)^3-(1-x)*sin((1-x)*pi))+1) ; [0.5, 1]
    /// </summary>
    static public double EaseInOutBack(double p)
    {
        if (p < 0.5f)
        {
            double f = 2 * p;
			return 0.5f * (f * f * f - f * Math.Sin(f * Mathf.PI));
        }
        else
        {
            double f = (1 - (2 * p - 1));
			return 0.5f * (1 - (f * f * f - f * Math.Sin(f * Mathf.PI))) + 0.5f;
        }
    }

    /// <summary>
    /// </summary>
    static public double EaseInBounce(double p)
    {
        return 1 - EaseOutBounce(1 - p);
    }

    /// <summary>
    /// </summary>
    static public double EaseOutBounce(double p)
    {
        if (p < 4 / 11.0f)
        {
            return (121 * p * p) / 16.0f;
        }
        else if (p < 8 / 11.0f)
        {
            return (363 / 40.0f * p * p) - (99 / 10.0f * p) + 17 / 5.0f;
        }
        else if (p < 9 / 10.0f)
        {
            return (4356 / 361.0f * p * p) - (35442 / 1805.0f * p) + 16061 / 1805.0f;
        }
        else
        {
            return (54 / 5.0f * p * p) - (513 / 25.0f * p) + 268 / 25.0f;
        }
    }

    /// <summary>
    /// </summary>
    static public double EaseInOutBounce(double p)
    {
        if (p < 0.5f)
        {
            return 0.5f * EaseInBounce(p * 2);
        }
        else
        {
            return 0.5f * EaseOutBounce(p * 2 - 1) + 0.5f;
        }
    }
}