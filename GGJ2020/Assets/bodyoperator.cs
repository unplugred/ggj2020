﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class bodyoperator : MonoBehaviour
{
	public int slot = -1;
	public static bodyoperator slot1;
	public static bodyoperator slot2;
	public static int bodycount;
	float anim = 0;
	public int state = 0;
	[SerializeField] GameObject forstdrp;
	public bool successful;
	public static bool bodified = true;
	public Image head;
	public static Stack successfulbodies = new Stack();

	void Start()
	{
		head.color = Color.HSVToRGB(Random.value, .6f, 1);
		if(slot1 == null)
		{
			slot = 0;
			slot1 = this;
		} else if(slot2 == null) {
			slot = 1;
			slot2 = this;
		}
	}

	void Update()
	{
		switch(state)
		{
			case 0:
				if(anim < 1)
				{
					if(slot >= 0)
					{
						anim = Mathf.Clamp01(anim + Time.deltaTime*.5f);
						if(slot == 0)
							transform.localPosition = new Vector3(Mathf.Lerp(92f, 40f, anim), transform.localPosition.y, transform.localPosition.z);
						else
							transform.localPosition = new Vector3(Mathf.Lerp(-92f, -40f, anim), transform.localPosition.y, transform.localPosition.z);
					}
				} else {
					forstdrp.SetActive(true);
					state = 1;
				}
				return;
			case 2:
				if(anim > 0)
				{
					if(slot >= 0)
					{
						anim = Mathf.Clamp01(anim - Time.deltaTime*.5f);
						if(slot == 0)
							transform.localPosition = new Vector3(Mathf.Lerp(120f, 40f, anim), transform.localPosition.y, transform.localPosition.z);
						else
							transform.localPosition = new Vector3(Mathf.Lerp(-120f, -40f, anim), transform.localPosition.y, transform.localPosition.z);
					}
				} else {
					if(timerr.timeleft <= 0) {
						if(slot == 1) Destroy(gameObject);
						else state = 5;
					} else {
						if(slot == 0) slot1 = null;
						else slot2 = null;
						Instantiate(uioperator.uiop.newbody, new Vector3(1000,transform.position.y,transform.position.z), Quaternion.identity, transform.parent);
						if(successful) {
							transform.localPosition = new Vector3(-120, transform.localPosition.y, transform.localPosition.z);
							successfulbodies.Push(gameObject);
							gameObject.SetActive(false);
						}
						else Destroy(gameObject);
						state = 3;
					}
				}
				return;
			case 3:
				transform.localPosition = new Vector3(transform.localPosition.x + Time.deltaTime*20, transform.localPosition.y, transform.localPosition.z);
				if(transform.localPosition.x >= 120) Destroy(gameObject);
				return;
			case 5:
				anim -= Time.deltaTime;
				if(anim <= 0) {
					if(successfulbodies.Count == 0) Destroy(gameObject);
					else (successfulbodies.Pop() as GameObject).SetActive(true);
					anim = 3;
				}
				return;
		}
	}

	public void diee()
	{
		if(state == 4) spawner.facestageamount--;
		uioperator.uiop.yesno[slot + 2].SetActive(true);
		state = 2;
		successful = false;
		uioperator.uiop.facecontrolleroids[slot].facecomplete(false);
		uioperator.uiop.negativeplay();
	}

	public void endd()
	{
		if(state == 4) spawner.facestageamount--;
		state = 2;
		successful = false;
		uioperator.uiop.facecontrolleroids[slot].facecomplete(false);
	}

	public void succeed()
	{
		uioperator.uiop.yesno[slot].SetActive(true);
		state = 2;
		successful = true;
		bodycount++;
		uioperator.uiop.currentbodies.text = bodycount.ToString();
		spawner.facestageamount--;
		uioperator.uiop.facecontrolleroids[slot].facecomplete(true);
		uioperator.uiop.positiveplay();
	}

	public void facify()
	{
		state = 4;
		uioperator.uiop.facecontrolleroids[slot].gameObject.SetActive(true);
		spawner.facestageamount++;
	}

	public void bodify()
	{
		if(bodified)
		{
			Instantiate(uioperator.uiop.newbody, new Vector3(1000, transform.position.y, transform.position.z), Quaternion.identity, transform.parent);
			bodified = false;
		}
	}
}