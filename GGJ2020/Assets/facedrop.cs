﻿using UnityEngine.UI;
using UnityEngine;

public class facedrop : MonoBehaviour
{
	public static facedrop currentdrop;
	public bodyoperator papa;
	public Transform fcthng;
	public Image img;
	bool lefteye = false;
	bool righteye = false;
	bool nose = false;
	bool mouth = false;
	bool hair = true;

	void OnEnable()
	{
		lefteye = false;
		righteye = false;
		nose = false;
		mouth = false;
		hair = true;
	}

	void OnMouseEnter()
	{
		if(limb.currentdrag == null) return;
		if(limb.currentdrag.phsx.Length == 2) return;
		currentdrop = this;
	}
	void OnMouseExit()
	{
		if(limb.currentdrag == null) return;
		if(limb.currentdrag.phsx.Length == 2) return;
		if(currentdrop == this) currentdrop = null;
	}
	public void donee(limbtype typ, Vector3 pos)
	{
		currentdrop = null;
		float points = -1;
		switch(typ)
		{
			case limbtype.eye: //+-3.3, 2.5
				if(pos.x >= 0)
				{
					if(righteye)
					{
						if(lefteye) {
							breakkkk();
						} else {
							points = 25;
							points -= Mathf.Abs(pos.x + 3.3f)*2.7f;
							points -= Mathf.Abs(pos.y - 2.5f)*2.7f;
							lefteye = true;
							if(hair && nose && mouth && lefteye && righteye) doneeeee();
						}
					} else {
						points = 25;
						points -= Mathf.Abs(pos.x - 3.3f)*2.7f;
						points -= Mathf.Abs(pos.y - 2.5f)*2.7f;
						righteye = true;
						if(hair && nose && mouth && lefteye && righteye) doneeeee();
					}
				}
				else{
					if(lefteye) {
						if(righteye) {
							breakkkk();
						} else {
							points = 25;
							points -= Mathf.Abs(pos.x - 3.3f)*2.7f;
							points -= Mathf.Abs(pos.y - 2.5f)*2.7f;
							righteye = true;
							if(hair && nose && mouth && lefteye && righteye) doneeeee();
						}
					} else {
						points = 25;
						points -= Mathf.Abs(pos.x + 3.3f)*2.7f;
						points -= Mathf.Abs(pos.y - 2.5f)*2.7f;
						lefteye = true;
						if(hair && nose && mouth && lefteye && righteye) doneeeee();
					}
				}
				break;
			case limbtype.nose: //0, 0
				if(nose) {
					breakkkk();
				} else {
					points = 25;
					points -= Mathf.Abs(pos.x)*2.7f;
					points -= Mathf.Abs(pos.y)*2.7f;
					nose = true;
					if(hair && nose && mouth && lefteye && righteye) doneeeee();
				}
				break;
			case limbtype.mouth: //0, -5
				if(mouth) {
					breakkkk();
				} else {
					points = 25;
					points -= Mathf.Abs(pos.x)*2.7f;
					points -= Mathf.Abs(pos.y + 5)*2.7f;
					mouth = true;
					if(hair && nose && mouth && lefteye && righteye) doneeeee();
				}
				break;
			case limbtype.hair:
				if(hair) {
					breakkkk();
				} else {
					hair = true;
					if(hair && nose && mouth && lefteye && righteye) doneeeee();
				}
				break;
		}
		if(points != -1) uioperator.uiop.textpoint(Mathf.RoundToInt(points));
	}
	public void doneeeee()
	{
		papa.succeed();
		enabled = false;
	}
	public void breakkkk()
	{
		papa.diee();
		enabled = false;
	}
}