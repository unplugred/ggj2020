﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wigglywiggle : MonoBehaviour
{
	bool off = true;
	float timer = 12;
	[SerializeField] limb parts;
	float[] offset;
	float[] speed;
	float[] startrot;

	void OnDisable()
	{
		off = false;
	}

	void OnEnable() {
		if(off) return;
		offset = new float[parts.phsx.Length];
		speed = new float[parts.phsx.Length];
		startrot = new float[parts.phsx.Length];
		for(int x = 0; x < parts.phsx.Length; x++)
		{
			offset[x] = Random.Range(0, Mathf.PI*2);
			speed[x] = Random.Range(3f, 5f);
			startrot[x] = parts.phsx[x].transform.localEulerAngles.z;
		}
		timer = 12;
	}

	void Update()
	{
		if(off) return;
		timer -= Time.deltaTime;
		if(timer <= 8f)
		{
			float amntt = (float)scr_easings.EaseInCubic(Mathf.Clamp(8f - timer,0,1.5f));
			for(int x = 0; x < parts.phsx.Length; x++)
				parts.phsx[x].transform.localEulerAngles = new Vector3(0, 0, Mathf.Sin(timer*speed[x] + offset[x])*7*amntt + startrot[x]);
		}
	}
}
