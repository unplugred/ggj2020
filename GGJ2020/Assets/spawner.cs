﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour
{
	[SerializeField] float spawnSpeed;
	[SerializeField] Transform parent;
	float nextspawn;
	[SerializeField] GameObject[] bodies;
	[SerializeField] GameObject[] hands;
	[SerializeField] GameObject[] legs;
	[SerializeField] GameObject[] mouths;
	[SerializeField] GameObject[] noses;
	[SerializeField] GameObject[] eyes;
	[SerializeField] GameObject[] hairs;
	public static int facestageamount = 0;

	void Update()
	{
		if(timerr.timeleft <= 0) {
			enabled = false;
		}
		nextspawn -= Time.deltaTime;
		while(nextspawn <= 0)
		{
			nextspawn += spawnSpeed;
			int part = 0;
			if(facestageamount == 0) part = Mathf.FloorToInt(Random.Range(0.5f, 2.99999f)); else
			if(facestageamount == 1) part = Mathf.FloorToInt(Random.Range(0.5f, 6.99999f)); else
			if(facestageamount == 2) part = Random.Range(3, 7);
			switch (part)
			{
				case 0:
					Instantiate(bodies[Random.Range(0, bodies.Length)], new Vector3(0, 0, 0), Quaternion.identity, parent).transform.localPosition = new Vector3(90, -32.5f, 0);
					break;
				case 1:
					Instantiate(hands[Random.Range(0, hands.Length)], new Vector3(0, 0, 0), Quaternion.identity, parent).transform.localPosition = new Vector3(90, -32.5f, 0);
					break;
				case 2:
					Instantiate(legs[Random.Range(0, legs.Length)], new Vector3(0, 0, 0), Quaternion.identity, parent).transform.localPosition = new Vector3(90, -32.5f, 0);
					break;
				case 3:
					Instantiate(mouths[Random.Range(0, mouths.Length)], new Vector3(0, 0, 0), Quaternion.identity, parent).transform.localPosition = new Vector3(90, -32.5f, -7);
					break;
				case 4:
					Instantiate(noses[Random.Range(0, noses.Length)], new Vector3(0, 0, 0), Quaternion.identity, parent).transform.localPosition = new Vector3(90, -32.5f, -7);
					break;
				case 5:
					Instantiate(eyes[Random.Range(0, eyes.Length)], new Vector3(0, 0, 0), Quaternion.identity, parent).transform.localPosition = new Vector3(90, -32.5f, -7);
					break;
				case 6:
					//Instantiate(hairs[Random.Range(0, hairs.Length)], new Vector3(0, 0, 0), Quaternion.identity, parent).transform.localPosition = new Vector3(90, -32.5f, -7);
					break;
			}
		}
	}
}
