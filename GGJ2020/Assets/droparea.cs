﻿using UnityEngine.UI;
using UnityEngine;

public class droparea : MonoBehaviour
{
	public static droparea currentdrop;
	public Image arrow;
	public limbtype lookingfor;
	public body nearestbody;
	public bodyoperator nearestperson;

	void OnEnable()
	{
		if(arrow.color != Color.white) gameObject.SetActive(false);
	}
	void OnMouseEnter()
	{
		if(limb.currentdrag == null) return;
		if(limb.currentdrag.phsx.Length == 1) return;
		if(currentdrop != null) currentdrop.arrow.color = Color.white;
		currentdrop = this;
		arrow.color = new Color(0.4159844f, 0.7169812f, 0.4696397f);
	}
	void OnMouseExit()
	{
		if(limb.currentdrag == null) return;
		if(limb.currentdrag.phsx.Length == 1) return;
		if(currentdrop == this) {
			currentdrop = null;
			arrow.color = Color.white;
		}
	}
	public void donee(bool success)
	{
		gameObject.SetActive(false);
		currentdrop = null;
		if(nearestperson == null) nearestperson = nearestbody.nearestoperator;
		if(success)
		{
			uioperator.uiop.textpoint(20);
			if(lookingfor == limbtype.body) nearestperson.bodify(); 
			nearestbody.enablestuff();
		}
		else {
			nearestperson.diee();
			uioperator.uiop.textpoint(-20);
		}
	}
}