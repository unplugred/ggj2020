﻿using UnityEngine.UI;
using UnityEngine;

public class timerr : MonoBehaviour
{
	[SerializeField] Text timetext;
	public static int points = 0;
	public static float timeleft = 180;


	void Start()
	{
		points = 0;
		timeleft = 180;
	}

	void Update()
	{
		timeleft -= Time.deltaTime;
		if(timeleft <= 0) {
			timetext.text = "0:00";
			uioperator.uiop.OutOfTime();
			if(bodyoperator.slot1 != null) bodyoperator.slot1.endd();
			if(bodyoperator.slot2 != null) bodyoperator.slot2.endd();
			enabled = false;
		} else {
			int bleh = Mathf.CeilToInt(timeleft);
			timetext.text = Mathf.FloorToInt(bleh/60) + ":" + (bleh%60).ToString().PadLeft(2, '0');
		}
	}
}
