﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class yesno : MonoBehaviour
{
	float flt;
	float timerr;

	void OnEnable()
	{
		timerr = 1.5f;
	}

	void Update()
	{
		timerr -= Time.deltaTime;
		flt = Mathf.Clamp01(flt + (timerr > 0 ? 1 : -1)*Time.deltaTime*3);
		float s = (float)scr_easings.EaseOutCubic(flt);
		transform.localScale = new Vector3(s,s,s);
		if(flt == 0) gameObject.SetActive(false);
	}
}
