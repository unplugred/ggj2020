﻿using UnityEngine.UI;
using UnityEngine;

public class limbphsx : MonoBehaviour
{
	[SerializeField] limb lamb;
	[SerializeField] byte id;
	[SerializeField] Image spr;

	void OnMouseEnter()
	{
		lamb.hovery(true);
	}
	void OnMouseExit()
	{
		lamb.hovery(false);
	}
	void OnMouseDown()
	{
		lamb.clicke(true, id);
	}
	void OnMouseUp()
	{
		lamb.clicke(false, id);
	}

	public void clr(Color clrr)
	{
		spr.color = clrr;
	}

	public void hvr(bool trfls)
	{
		spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, trfls ? .9f : 1);
	}
}
