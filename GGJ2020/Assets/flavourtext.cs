﻿using UnityEngine.UI;
using UnityEngine;

public class flavourtext : MonoBehaviour
{
	[SerializeField] Text text;
	public static int txtt;

	void Start()
	{
		if(txtt >= 0) text.text = "+" + txtt;
		else text.text = txtt.ToString();
		transform.localPosition = new Vector3(
			(Input.mousePosition.x*160.0f)/(float)Screen.width - 80,
			(Input.mousePosition.y*90.0f)/(float)Screen.height - 45, -7.3f);
	}

	void Update()
	{
		text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a - Time.deltaTime);
		transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y + Time.deltaTime*10, transform.localPosition.z);
		if(text.color.a <= 0) Destroy(gameObject);
	}
}