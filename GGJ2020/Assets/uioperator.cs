﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using System;

public class uioperator : MonoBehaviour
{
	public static uioperator uiop;
	public GameObject[] yesno;
	public GameObject newbody;
	public Text currentbodies;
	public facescreen[] facecontrolleroids;
	[SerializeField] Image panel;
	[SerializeField] Image endingplack;
	[SerializeField] Image fadeout;
	[SerializeField] Text endingtext;
	[SerializeField] Text successfail;
	[SerializeField] GameObject pointstext;
	int state = 0;
	float timer = 1;
	bool quit = false;
	[Space()]
	[SerializeField] AudioSource sauce;
	[SerializeField] AudioClip[] squishes;
	[SerializeField] AudioClip click;
	[SerializeField] AudioSource announcement;
	[SerializeField] AudioClip[] positive;
	[SerializeField] AudioClip[] negative;

	void Start()
	{
		uiop = this;
		fadeout.color = new Color(0,0,0,1);
	}

	void Update()
	{
		switch(state)
		{
			case 0:
				timer = Mathf.Clamp01(timer - Time.deltaTime);
				float animm = (float)scr_easings.EaseInOutSine(timer);
				fadeout.color = new Color(0,0,0,animm);
				if(timer >= 1) {
					timer = 0;
					state = 4;
				}
				break;
			case 1:
				timer = Mathf.Clamp01(timer - Time.deltaTime*.5f);
				animm = (float)scr_easings.EaseInCubic(timer);
				endingplack.transform.localPosition = new Vector3(0, 60*animm, -2);
				panel.transform.localPosition = new Vector3(0, 20*(1 - animm) + 45, -1);
				if(timer <= 0) state = 3;
				break;
			case 2:
				timer = Mathf.Clamp01(timer + Time.deltaTime);
				animm = (float)scr_easings.EaseInOutSine(timer);
				fadeout.color = new Color(0,0,0,animm);
				if(timer == 1)
				{
					if(quit) Application.Quit();
					else {
						SceneManager.LoadScene(0);
						bodyoperator.slot1 = null;
						bodyoperator.slot2 = null;
						bodyoperator.bodycount = 0;
						bodyoperator.bodified = true;
						droparea.currentdrop = null;
						facedrop.currentdrop = null;
						limb.currentdrag = null;
						spawner.facestageamount = 0;
						bodyoperator.successfulbodies.Clear();
					}
				}
				break;
		}
	}

	public void OutOfTime()
	{
		state = 1;
		timer = 1;
		endingtext.text = "Bodies: " + currentbodies.text + "\nScore: " + timerr.points;
		successfail.text = Int32.Parse(currentbodies.text) > 6 ? "Success!" : "Failed.";
	}

	public void restartt()
	{
		state = 2;
		quit = false;
	}

	public void quitt()
	{
		state = 2;
		quit = true;
	}

	public void squishplay()
	{
		sauce.clip = squishes[UnityEngine.Random.Range(0, squishes.Length)];
		sauce.Play();
	}

	public void clickplay()
	{
		sauce.clip = click;
		sauce.Play();
	}

	public void positiveplay()
	{
		announcement.clip = positive[UnityEngine.Random.Range(0, positive.Length)];
		announcement.Play();
	}

	public void negativeplay()
	{
		announcement.clip = negative[UnityEngine.Random.Range(0, negative.Length)];
		announcement.Play();
	}

	public void textpoint(int pt)
	{
		flavourtext.txtt = pt;
		timerr.points += pt;
		Instantiate(pointstext, Vector3.zero, Quaternion.identity, transform);
	}
}